import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import reducerAuth from './reducerAuth';
import reducerChat from './reducerChat';
import reducerClient from './reducerClient';

const reducer = combineReducers({
  form: formReducer,
  reducerAuth,
  reducerChat,
  reducerClient
});
export default reducer;
