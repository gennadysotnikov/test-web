import * as actionTypes from '../actions/auth/indexActionTypes';

export const initialState = {
  auth: false,
  massage: 'Добро пожаловать!',
  jwt: ''
};
const reducerAuth = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHENTICATION: {
      return Object.assign({}, state, { massage: '' });
    }
    case actionTypes.AUTHENTICATION_SUCCEEDED: {
      return Object.assign({}, state, {
        massage: action.massage,
        jwt: action.jwt,
        auth: action.auth
      });
    }
    case actionTypes.AUTHENTICATION_FAILED: {
      return Object.assign({}, state, {
        massage: action.massage,
        auth: action.auth,
        jwt: ''
      });
    }

    case actionTypes.EXIT: {
      return Object.assign({}, state, {
        massage: action.massage,
        auth: action.auth,
        jwt: ''
      });
    }
    default:
      return state;
  }
};
export default reducerAuth;
