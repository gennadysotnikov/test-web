import * as actionTypes from '../actions/chat/chatActionTypes.js';
const initialState = {
  chats: [],
  hasMore: true,
  lastMassageIndex: 0,
  VisibleCreateChat: false,
  VisibleChatWindow: true,
  isSearch: false,
  searchResult: {},
  notFoundMassage: false,
  chatMassages: [],
  showChat: false,
  showChatStatus: 'none'
};

const reducerChat = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CHATS: {
      return Object.assign({}, state, { chats: action.chats });
    }
    case actionTypes.SET_CHAT_STATUS: {
      return Object.assign({}, state, {
        showChatStatus: action.showChatStatus,
        hasMore: true,
        chats: []
      });
    }
    case actionTypes.CREATE_CHAT: {
      return Object.assign({}, state, {
        chats: action.chats
      });
    }
    case actionTypes.TOGGLE_CREATE_CHAT: {
      return Object.assign({}, state, {
        VisibleChatWindow: action.VisibleChatWindow,
        VisibleCreateChat: action.VisibleCreateChat
      });
    }
    case actionTypes.SEARCH_TOGGLE: {
      return Object.assign({}, state, {
        isSearch: action.isSearch
      });
    }
    case actionTypes.SET_SEARCH_RESULT: {
      return Object.assign({}, state, {
        searchResult: action.searchResult,
        isSearch: action.isSearch
      });
    }
    case actionTypes.MASSAGE_TOGGLE: {
      return Object.assign({}, state, {
        notFoundMassage: action.notFoundMassage
      });
    }
    case actionTypes.SHOW_CHAT: {
      return Object.assign({}, state, {
        showChat: true,
        chatMassages: action.chatMassages,
        VisibleChatWindow: false,
        openedChatId: action.openedChatId
      });
    }
    case actionTypes.EXIT_CHAT: {
      return Object.assign({}, state, {
        showChat: false,
        chatMassages: [],
        VisibleChatWindow: true
      });
    }
    case actionTypes.ADD_MASSAGE: {
      return Object.assign({}, state, {
        chatMassages: action.chatMassages
      });
    }
    case actionTypes.TOGGLE_HAS_MORE: {
      return Object.assign({}, state, {
        hasMore: action.hasMore
      });
    }
    default:
      return state;
  }
};
export default reducerChat;
