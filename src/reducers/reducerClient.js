import * as actionTypes from '../actions/client/clientActionTypes';

export const initialState = {
  isQuestion: false,
  inLine: false,
  DialogFinished: false
};
const reducerClient = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.IS_QUESTION_TOGGLE: {
      return Object.assign({}, state, {
        inLine: action.inLine,
        isQuestion: action.isQuestion
      });
    }
    default:
      return state;
  }
};
export default reducerClient;
