import { baseUrl, getAuthReq } from '../../queryConfig';
import { queryWithJsonOutput } from '../../jwtAuthFunctions';
import * as actionTypes from './clientActionTypes';

export const createChatQuery = (object, jwt) => dispatch => {
  let url =
    baseUrl +
    '/create?name=' +
    object.subtopic +
    '&clientName=' +
    object.clientName;
  queryWithJsonOutput(url, getAuthReq(jwt)).then(() => {
    dispatch(isQuestionToggle(true, true));
  });
};
export const isQuestionToggle = (isQuestion, inLine) => ({
  type: actionTypes.IS_QUESTION_TOGGLE,
  isQuestion: isQuestion,
  inLine: inLine
});
