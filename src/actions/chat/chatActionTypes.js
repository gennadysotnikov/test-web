export const CREATE_CHAT = 'CREATE_CHAT';
export const TOGGLE_CREATE_CHAT = 'TOGGLE_CREATE_CHAT';
export const SET_SEARCH_RESULT = 'SET_SEARCH_RESULT';
export const SEARCH_TOGGLE = 'SEARCH_TOGGLE';
export const MASSAGE_TOGGLE = 'MASSAGE_TOGGLE';
export const SHOW_CHAT = 'SHOW_CHAT';
export const EXIT_CHAT = 'EXIT_CHAT';
export const ADD_MASSAGE = 'ADD_MASSAGE';
export const GET_CHATS = 'GET_CHATS';
export const TOGGLE_SHOW_CHAT_STATUS = 'TOGGLE_SHOW_CHAT_STATUS';
export const TOGGLE_HAS_MORE = 'TOGGLE_HAS_MORE';
export const SET_CHAT_STATUS = 'SET_CHAT_STATUS';
export const TOGGLE_CHATS = 'TOGGLE_CHATS';
