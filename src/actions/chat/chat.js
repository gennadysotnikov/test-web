import { store } from '../../store/store';
import { bindActionCreators } from 'redux';
import { queryWithJsonOutput } from '../../jwtAuthFunctions';
import { baseUrl, getAuthReq } from '../../queryConfig';
import * as actionTypes from './chatActionTypes';

export const getChatsSuccess = chats => {
  return { type: actionTypes.GET_CHATS, chats };
};
export const toggleChats = showDialogWindow => {
  return { type: actionTypes.TOGGLE_CHATS, showChatsWindow: showDialogWindow };
};
export const toggleHasMore = hasMore => {
  return { type: actionTypes.TOGGLE_HAS_MORE, hasMore: hasMore };
};
export const createChat = chats => {
  return {
    type: actionTypes.CREATE_CHAT,
    chats,
    VisibleChatWindow: true,
    VisibleCreateChat: false
  };
};
export const toggleCreateChat = (VisibleChatWindow, VisibleCreateChat) => {
  return {
    type: actionTypes.TOGGLE_CREATE_CHAT,
    VisibleChatWindow,
    VisibleCreateChat
  };
};
export const setChatStatus = newChatStatus => ({
  type: actionTypes.SET_CHAT_STATUS,
  showChatStatus: newChatStatus
});

export const toggleShowChatStatus = (newChatStatus, jwt, chats) => dispatch => {
  dispatch(getChatQuery(jwt, 0, newChatStatus, chats));
  dispatch(setChatStatus(newChatStatus));
};

export const createChatQuery = (e, jwt) => dispatch => {
  e.preventDefault();
  let url = baseUrl + '/create?name=' + e.target[0].value;
  queryWithJsonOutput(url, getAuthReq(jwt)).then(() => {
    dispatch(toggleCreateChat(true, false));
  });
};

export const getChatQuery = (jwt, index, status, chats) => dispatch => {
  let url = baseUrl + '?Index=' + index + '&status=' + status;
  queryWithJsonOutput(url, getAuthReq(jwt)).then(res => {
    if (res.length > 0) {
      return dispatch(getChatsSuccess(Array.from(chats).concat(res)));
    } else return dispatch(toggleHasMore(false));
  });
};

export const setSearchResult = result => {
  //result верный
  return {
    type: actionTypes.SET_SEARCH_RESULT,
    searchResult: result,
    isSearch: true,
    VisibleChatWindow: false
  };
};

export const searchToggle = isSearch => {
  return {
    type: actionTypes.SEARCH_TOGGLE,
    isSearch
  };
};

export const massageToggle = notFoundMassage => {
  return {
    type: actionTypes.MASSAGE_TOGGLE,
    notFoundMassage: notFoundMassage
  };
};

export const getSearchResult = (searchStr, jwt) => dispatch => {
  let url = baseUrl + '/findMassages?searchStr=' + searchStr;
  queryWithJsonOutput(url, getAuthReq(jwt)).then(res =>
    dispatch(setSearchResult(res))
  );
};

export const showMassages = (chatMassages, openedChatId) => {
  return {
    type: actionTypes.SHOW_CHAT,
    chatMassages: chatMassages,
    openedChatId: openedChatId
  };
};
export const exitChat = () => {
  return {
    type: actionTypes.EXIT_CHAT
  };
};
export const getMassages = (
  chatId,
  jwt,
  earliestMassageIndex,
  massages
) => dispatch => {
  let url =
    baseUrl +
    '/getMassages?chatId=' +
    chatId +
    '&massageIndex=' +
    earliestMassageIndex;
  queryWithJsonOutput(url, getAuthReq(jwt)).then(
    (res) /*в res находится массив всех ссобщений*/ => {
      if (JSON.stringify(res) === '[]') {
        alert('toggleHasMore сейчас сработает');
        dispatch(toggleHasMore(false));
        return;
      }
      if (massages === undefined) {
        alert('getMassages ветвь if');
        dispatch(showMassages(res, chatId));
      } else {
        alert('getMassages res=' + JSON.stringify(res));
        res = Array.from(res);
        res = res.concat(Array.from(massages));
        alert('getMassages after concat res=' + JSON.stringify(res));
        dispatch(showMassages(res, chatId));
      }
    }
  );
};
export const addMassage = (massage, chatMassages) => {
  chatMassages = Array.from(chatMassages);
  chatMassages.push(massage);
  return {
    type: actionTypes.ADD_MASSAGE,
    chatMassages: chatMassages
  };
};

export const sendMassage = (chatId, content, jwt, allMassages) => dispatch => {
  if (content !== '') {
    let url = baseUrl + '/sendMassage?chatId=' + chatId + '&content=' + content;
    //all massages тут есть
    queryWithJsonOutput(url, getAuthReq(jwt)).then(res =>
      dispatch(addMassage(res, allMassages))
    );
  } else alert('Вы не можете отправить пустое сообщение');
};

export const chatAction = bindActionCreators(
  {
    getChatsSuccess,
    toggleCreateChat,
    toggleChats,
    createChat,
    setSearchResult
  },
  store.dispatch
);
export default chatAction;
