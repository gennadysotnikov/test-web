import { store } from '../../store/store';
import { bindActionCreators } from 'redux';
import { queryGet, queryWithTextOutput } from '../../jwtAuthFunctions';
import { SubmissionError } from 'redux-form';
import { baseUrlUser, getAuthReq } from '../../queryConfig';
import { registration } from '../../authorization/forms/registerForm';

//action creators
export const authentification = () => {
  return {
    type: 'AUTHENTICATION',
    massage: 'Добро пожаловать',
    auth: false,
    jwt: ''
  };
};
export const authentificationSuccess = jwt => {
  return {
    type: 'AUTHENTICATION_SUCCEEDED',
    massage: 'Success',
    jwt: jwt,
    auth: true
  };
};
export const authentificationFailed = () => {
  return {
    type: 'AUTHENTICATION_FAILED',
    massage: 'Failed',
    auth: false,
    jwt: ''
  };
};

export const firstCheckQuery = jwt => dispatch => {
  let url = baseUrlUser + '/jwt';
  queryWithTextOutput(url, getAuthReq(jwt)).then(res => {
    return res !== 'Incorrect'
      ? dispatch(authentificationSuccess(jwt))
      : dispatch(authentificationFailed);
  });
};
//из-за того, что redux form плохо работает с диспатчем мы не будем
// передавать диспатч как в других экшинах
export const submit = values => {
  let url = baseUrlUser + '/login';
  const req = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      email: values.email,
      password: values.password
    })
  };
  return queryWithTextOutput(url, req).then(res => {
    if (res != '') {
      action.success(res);
    } else {
      throw new SubmissionError({
        email: 'Логин, или пароль неверны!',
        _error: 'Login failed \n'
      });
    }
  });
};

export const exit = () => {
  return {
    type: 'EXIT',
    massage: 'Добро пожаловать',
    auth: false,
    jwt: ''
  };
};
export const authOrReg = (socialNetwork, response) => {
  const email =
    socialNetwork === 'google'
      ? response.profileObj !== undefined && response.profileObj.email
      : response.session.user.domain + '@vk.ru';
  const url = baseUrlUser + '/isUser/?email=' + email;
  alert('url=' + url);
  queryGet(url).then(res => {
    if (res !== '') {
      action.success(res);
    } else {
      registration({
        email: email,
        userName:
          socialNetwork === 'google'
            ? email.slice(0, email.indexOf('@'))
            : response.session.user.domain,
        password: 'adsdofsd[pafjas[odf,cCD[SD12390124394OIA,xaDAd'
      });
    }
  });
};

export const action = bindActionCreators(
  {
    auth: authentification,
    success: authentificationSuccess,
    fail: authentificationFailed,
    exit: exit
  },
  store.dispatch
);
export default action;
