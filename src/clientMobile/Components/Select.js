import React from 'react';
import { connect } from 'formik';
const Select = props => {
  switch (props.formik.values.topic) {
    case 'Topic1':
      return (
        <select>
          <option value="Subtopic1"> Подтема 1 Темы 1 </option>
          <option value="Subtopic2"> Подтема 2 Темы 1 </option>
          <option value="Subtopic3"> Подтема 3 Темы 1 </option>
        </select>
      );
    case 'Topic2':
      return (
        <select>
          <option value="Subtopic1"> Подтема 1 Темы 2 </option>
          <option value="Subtopic2"> Подтема 2 Темы 2 </option>
          <option value="Subtopic3"> Подтема 3 Темы 2 </option>
        </select>
      );
    case 'Topic3':
      return (
        <select>
          <option value="Subtopic1"> Подтема 1 Темы 3 </option>
          <option value="Subtopic2"> Подтема 2 Темы 3 </option>
          <option value="Subtopic3"> Подтема 3 Темы 3 </option>
        </select>
      );
    default:
      return <div> {props.formik.values.topic} </div>;
  }
};
export default connect(Select);
