import React from 'react';
import { auth } from '../сss_modules/auth.module.scss';
/*import renderField from '../components/renderField';*/
import { Formik, Field, Form } from 'formik';
import Select from './Components/Select';
import { store } from '../store/store';
import { createChatQuery } from '../actions/client/actions';

function StartPage(props) {
  const jwt = props.jwt;
  return (
    <Formik
      onSubmit={values => {
        alert(JSON.stringify(values));
        store.dispatch(createChatQuery(values, jwt));
      }}
      initialValues={{ userName: '', topic: 'Topic1', subtopic: 'Topic2' }}
      render={() => {
        return (
          <Form className={auth}>
            <h2> Задайте свой вопрос </h2>
            <label> Введите имя </label>
            <Field
              name="clientName"
              key="clientName"
              component="input"
              type="text"
            />
            <label>Выберите тему обращения</label>
            <Field name="topic" component="select">
              <option value="Topic1"> Тема 1 </option>
              <option value="Topic2"> Тема 2 </option>
              <option value="Topic3"> Тема 3 </option>
            </Field>
            <label>Выберите подтему</label>
            <Field name="subtopic" component={Select}></Field>
            <button
              type="submit"
              style={{
                width: '100%',
                marginLeft: '0%',
                marginTop: '10px',
                fontSize: '13pt'
              }}
            >
              Задать вопрос
            </button>
          </Form>
        );
      }}
    />
  );
  /* <Form
    onSubmit={() => {
    alert('Вопрос задан');
    /!* функция которая отправляет запрос в базу и переводит пользователя в очередь*!/
    }}
    className={auth}
    >
    <h2> Задайте свой вопрос </h2>
    <label> Введите имя </label>
    <Field
    name="userName"
    key="userName"
    component={renderField}
    type="text"
    />
    <label>Выберите тему обращения</label>
    <Field name="topic" component="select">
    <option value="Topic1"> Тема 1 </option>
    <option value="Topic2"> Тема 2 </option>
    <option value="Topic3"> Тема 3 </option>
    </Field>
    <label>Выберите подтему</label>
    <Field name="subtopic" component="select">
    {values => {
    switch (values.topic) {
    case 'Topic1':
    return (
    <>
    <option> Подтема 1 Темы 1 </option>
    <option> Подтема 2 Темы 1 </option>
    <option> Подтема 3 Темы 1 </option>
    </>
    );
    case 'Topic2':
    return (
    <>
    <option> Подтема 1 Темы 2 </option>
    <option> Подтема 2 Темы 2 </option>
    <option> Подтема 3 Темы 2 </option>
    </>
    );
    case 'Topic3':
    return (
    <>
    <option> Подтема 1 Темы 3 </option>
    <option> Подтема 2 Темы 3 </option>
    <option> Подтема 3 Темы 3 </option>
    </>
    );
    }
    }}
    </Field>
    {error && <strong>{error}</strong>}
    <button
    type="submit"
    style={{
    width: '100%',
    marginLeft: '0%',
    marginTop: '10px',
    fontSize: '13pt'
    }}
    >
    Задать вопрос
    </button>
    </Form>
    */
}
export default StartPage;
