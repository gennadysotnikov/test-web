import React from 'react';
import StartPage from './StartPage';
import InLine from './InLine';
import Estimate from './Estimate';
import Dialog from './Dialog';
import { connect } from 'react-redux';
const mapStateToProps = state => ({
  isQuestion: state.reducerClient.isQuestion,
  inLine: state.reducerClient.inLine,
  DialogFinished: state.reducerClient.DialogFinished,
  jwt: state.reducerAuth.jwt
});

function MainPage({ jwt, isQuestion, inLine, DialogFinished }) {
  return (
    <div>
      {!isQuestion ? (
        <StartPage jwt={jwt} />
      ) : inLine ? (
        <InLine />
      ) : DialogFinished ? (
        Estimate
      ) : (
        <Dialog />
      )}
    </div>
  );
}

export default connect(
  mapStateToProps,
  null
)(MainPage);
