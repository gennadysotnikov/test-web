export const getAuthReq = jwt => {
  return {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + jwt,
      'Content-Type': 'application/json;charset=UTF-8'
    })
  };
};
export const baseUrl = 'http://localhost:3000/chat';
export const baseUrlUser = 'http://localhost:3000/users';
