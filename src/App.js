import React from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from './store/store';
import styles from './сss_modules/app.module.scss';
import { PersistGate } from 'redux-persist/lib/integration/react';
import LoadingView from './components/LoadingView';
import StartPage from './Routes/StartPage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Error404Page from './Routes/Error404Page';
import RegistrationPage from './Routes/RegistrationPage';
import PasswordResetForm from './authorization/forms/passwordReset';
import ForgotPasswordForm from './authorization/forms/forgotPassword';
import MainPage from './clientMobile/MainPage';

class App extends React.Component {
  render() {
    return (
      <>
        <Provider store={store}>
          <PersistGate loading={<LoadingView />} persistor={persistor}>
            <div className={styles.backgroundBlur} />
            <div className={styles.AppHeader}>
              <Router>
                <Switch>
                  <Route exact path="/" component={StartPage} />
                  <Route
                    exact
                    path="/registration"
                    component={RegistrationPage}
                  />
                  <Route path="/prf" component={PasswordResetForm} />
                  <Route path="/fpf" component={ForgotPasswordForm} />
                  <Route path="/client" component={MainPage} />
                  <Route component={Error404Page} />
                </Switch>
              </Router>
            </div>
            <div />
          </PersistGate>
        </Provider>
      </>
    );
  }
}

export default App;
