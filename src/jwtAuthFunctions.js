export const queryWithJsonOutput = async (url, req) => {
  return fetch(url, req).then(res => (res = res.json()));
};

export const queryWithTextOutput = async (url, req) => {
  return fetch(url, req).then(res => (res = res.text()));
};

export const queryGet = async url => {
  return fetch(url).then(res => (res = res.text()));
};
