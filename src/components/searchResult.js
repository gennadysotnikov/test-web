import React from 'react';
import UL from './UL';

function SearchResult(props) {
  return (
    <>
      Чаты:
      <UL arrForShow={props.nameMatches} propOfObj="name" />
      Сообщения:
      <UL arrForShow={props.massageMatches} propOfObj="content" />
      Пользователи
      <UL arrForShow={props.userNameMatches} propOfObj="username" />
    </>
  );
}

export default SearchResult;
