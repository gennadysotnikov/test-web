import imgVK from '../img/3.png';
import AuthIMG from './AuthIMG';
import React from 'react';
import VkAuth from 'react-vk-auth';
import { authOrReg } from '../actions/auth';

const VKRegOrAuth = () => (
  <span>
    <VkAuth
      apiId="7121214"
      render={<AuthIMG img={imgVK} style={{ margin: '3px', float: 'left' }} />}
      callback={data => authOrReg('vk', data)}
      style={{
        border: '0',
        background: 'transparent',
        margin: '-2px',
        float: 'left'
      }}
    >
      <AuthIMG img={imgVK} />
    </VkAuth>
  </span>
);

export default VKRegOrAuth;
