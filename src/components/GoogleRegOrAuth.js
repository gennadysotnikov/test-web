import { authOrReg } from '../actions/auth';
import AuthIMG from './AuthIMG';
import imgGoogle from '../img/google.png';
import GoogleLogin from 'react-google-login';
import React from 'react';

const GoogleRegOrAuth = () => (
  <GoogleLogin
    clientId="903713015118-c1jif1kklqs09tsbo0mccu2r91innsdh.apps.googleusercontent.com"
    onSuccess={response => {
      authOrReg('google', response);
    }}
    render={renderProps => (
      <AuthIMG
        img={imgGoogle}
        onClick={renderProps.onClick}
        style={{ margin: '3px' }}
      />
    )}
    onFailure={() => alert('Вход через Google не удался')}
    cookiePolicy={'single_host_origin'}
  />
);
export default GoogleRegOrAuth;
