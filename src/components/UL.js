import React from 'react';
import '../сss_modules/ul.module.scss';
import LiWithDispatch from './LiWithDispatch';
import { InfiniteScroll } from 'react-infinite-scroll-component';
/*
import InfiniteScroll from 'react-infinite-scroll-component';
*/

//на вход получаем массив чего-то, на выходе список
function UL(props) {
  const arrForShow = props.arrForShow;
  const propOfObj = props.propOfObj;
  const idSource = props.idSourse;
  const liWithDispatch = props.LiWithDispatch; //сюда можно кинуть например true
  const actions = props.actions;
  const actionName = props.actionName;
  const jwt = props.jwt;
  const status = props.status;
  /*  const hasMore = props.hasMore;
  const loadMore = props.loadMore;*/
  if (
    liWithDispatch !== undefined &&
    actions !== undefined &&
    actionName !== undefined
  ) {
    return (
      <ul>
        <InfiniteScroll useWindow={false} pageStart={0}>
          {arrForShow.map((item, index) => {
            return (
              item.status === status && (
                <LiWithDispatch
                  key={idSource !== undefined ? item[idSource] : index}
                  id={idSource !== undefined ? item[idSource] : index}
                  content={item[propOfObj]}
                  actions={actions}
                  actionName={actionName}
                  jwt={jwt}
                  status={status}
                />
              )
            );
          })}
        </InfiniteScroll>
      </ul>
    );
  }
}

export default UL;
