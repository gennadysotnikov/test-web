import action from '../../actions/auth';
import React from 'react';

function ExitButton() {
  return <button onClick={action.exit}> Выйти </button>;
}
export default ExitButton;
