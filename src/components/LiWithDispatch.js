/*Компонент переиспользуемый (должен быть)*/
import React from 'react';
import OneChat from './oneChat';

function LiWithDispatch(props) {
  let actionName = props.actionName;
  /*let content = props.content;*/
  return (
    <li
      id={props.id}
      onClick={() => {
        props.actions[actionName](props.id, props.jwt);
      }}
    >
      <OneChat
        client={props.client}
        topic={props.topic}
        status={props.status}
      />
      {/*{content}*/}
    </li>
  );
}
export default LiWithDispatch;
