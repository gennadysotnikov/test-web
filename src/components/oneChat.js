import React from 'react';
import Container from 'reactstrap/es/Container';
import Row from 'reactstrap/es/Row';
import Col from 'reactstrap/es/Col';
import pic from '../img/u32.png';

const OneChat = props => (
  <Container style={{ fontSize: '12pt' }}>
    <Row>
      <Col sm="3">
        <img src={pic} width="45%" />
        <br />
        {props.client}
      </Col>
      <Col sm="7">{props.topic}</Col>
      <Col sm="2">
        <Col>last activity</Col>
        <Col>
          {props.status === 'active' && 'Сохранить'}
          {props.status === 'completed' && 'Удалить'}
        </Col>
      </Col>
    </Row>
  </Container>
);
export default OneChat;
