import React from 'react';
function AuthIMG(props) {
  return (
    <img
      srcSet={props.img}
      alt="Не получилось не фартануло"
      width="30px"
      style={props.style}
      onClick={props.onClick}
    />
  );
}
export default AuthIMG;
