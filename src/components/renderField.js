import React from 'react';
// import { enterError } from '../сss_modules/error.module.scss';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div style={{ marginTop: '5px' }}>
    <label>{label}</label>
    <div>
      <input
        {...input}
        placeholder={label}
        type={type}
        style={{ width: '100%' }}
      />
      {touched && (error && <div className={''}>{error}</div>)}
    </div>
  </div>
);
export default renderField;
