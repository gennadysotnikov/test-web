import React from 'react';
import { createChatQuery, toggleCreateChat } from '../../actions/chat/chat';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  chats: state.reducerChat.chats,
  jwt: state.reducerAuth.jwt
});

function CreateChatForm({ onCreateChat, jwt }) {
  return (
    <form onSubmit={e => onCreateChat(e, jwt)}>
      <input name="chatName" type="text" />
      <button type="submit"> Создать </button>
    </form>
  );
}
export default connect(
  mapStateToProps,
  dispatch => ({
    onCreateChat: (e, jwt) => {
      dispatch(createChatQuery(e, jwt));
    },
    onCloseChatCall: () => {
      dispatch(toggleCreateChat(true, false));
    }
  })
)(CreateChatForm);
