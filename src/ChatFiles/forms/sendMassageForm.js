import React from 'react';
import { connect } from 'react-redux';
import { sendMassage } from '../../actions/chat/chat';

const mapStateToProps = state => ({
  openedChatId: state.reducerChat.openedChatId,
  jwt: state.reducerAuth.jwt,
  chatMassages: state.reducerChat.chatMassages
});

const mapDispatchToProps = () => dispatch => ({
  onSendMassage: (chatId, content, jwt, chatMassages) => {
    dispatch(sendMassage(chatId, content, jwt, chatMassages));
  }
});

function SandMassageForm({ openedChatId, onSendMassage, jwt, chatMassages }) {
  const form = (
    <form
      onSubmit={() => {
        onSendMassage(
          openedChatId,
          document.getElementById('massageContent') !== null
            ? document.getElementById('massageContent').value
            : '',
          jwt,
          chatMassages
        );
      }}
    >
      <input type="text" id="massageContent" />
      <button type="submit">Отправить</button>
    </form>
  );
  return form;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SandMassageForm);
