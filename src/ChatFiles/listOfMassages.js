import InfiniteScroll from 'react-infinite-scroll-component';
import React from 'react';
import { baseUrl, getAuthReq } from '../queryConfig';
import { queryWithJsonOutput } from '../jwtAuthFunctions';
/*import { connect } from 'react-redux';
import { getMassages } from '../actions/chat';*/

/*(
    this.props.openedChatId,
        this.props.jwt,
        this.state.chatMassages[0] === undefined
            ? 0
            : this.state.chatMassages[0].massageId,
        this.state.chatMassages
)*/

class ListOfMassages extends React.Component {
  onLoadMassages = () => {
    let url =
      baseUrl +
      '/getMassages?chatId=' +
      this.props.openedChatId +
      '&massageIndex=' +
      (this.state.chatMassages[0] === undefined
        ? 0
        : this.state.chatMassages[0].massageId);
    queryWithJsonOutput(url, getAuthReq(this.props.jwt)).then(
      (res) /*в res находится массив всех ссобщений*/ => {
        if (JSON.stringify(res) === '[]') {
          if (this.state.hasMore !== false) this.setState({ hasMore: false });
          return;
        }
        if (this.state.chatMassages === undefined) {
          this.setState({ chatMassages: Array.from(res) });
        } else {
          res = Array.from(res);
          res = res.concat(Array.from(this.state.chatMassages));
          this.setState({ chatMassages: Array.from(res) });
        }
      }
    );
  };
  constructor(props) {
    super(props);
    this.state = {
      chatMassages: this.props.chatMassages, //здесь не должен быть пустой массив
      hasMore: props.hasMore
    };
  }
  render() {
    /* для корректной работы требуется чтобы в файле index.js библиотеки react-infinite-scroll-component
     * строка var atBottom = this.isElementAtBottom(target, this.props.scrollThreshold);
     * стала var atBottom = !(this.isElementAtBottom(target, this.props.scrollThreshold));
     *  */
    return (
      <ul id="list_of_massages" style={{ height: '100%', overflow: 'auto' }}>
        <InfiniteScroll
          dataLength={this.state.chatMassages.length}
          hasMore={this.state.hasMore}
          next={this.onLoadMassages.bind(this)}
          loader={<h4>Loading...</h4>}
          scrollableTarget="list_of_massages"
          upScroll={true}
          initialScrollY={200}
        >
          {this.state.chatMassages.map((item, index) => (
            <div key={index} id={index}>
              {item['content']}
            </div>
          ))}
        </InfiniteScroll>
      </ul>
    );
  }
}
export default ListOfMassages;
