import React from 'react';
import { connect } from 'react-redux';
import { exitChat, getMassages } from '../actions/chat/chat';
import SandMassageForm from './forms/sendMassageForm';
/*import Moment from 'react-moment';*/
import moment from 'moment';
import 'moment/locale/ru';
import Row from 'reactstrap/es/Row';
import Col from 'reactstrap/es/Col';
import ListOfMassages from './listOfMassages';

const mapStateToProps = state => {
  return {
    hasMore: state.reducerChat.hasMore,
    jwt: state.reducerAuth.jwt,
    openedChatId: state.reducerChat.openedChatId,
    chatMassages: state.reducerChat.chatMassages
  };
};
const mapDispatchToProps = dispatch => ({
  onExit: () => {
    dispatch(exitChat());
  },
  onFirstLoadMassages: (chatId, jwt, earliestMassageIndex, chatMassages) => {
    dispatch(getMassages(chatId, jwt, earliestMassageIndex, chatMassages));
  }
});

const Dialog = ({
  jwt,
  hasMore,
  openedChatId,
  onFirstLoadMassages,
  chatMassages
}) => {
  const lastActivity =
    chatMassages.length > 0 &&
    moment(
      Array.from(chatMassages)[Array.from(chatMassages).length - 1].dateCreate
    ).fromNow();

  if (chatMassages.length === 0) {
    onFirstLoadMassages(openedChatId, jwt, 0, chatMassages);
  }
  return (
    <>
      <Col
        style={{
          display: 'flex',
          width: '100%',
          flexDirection: 'column',
          height: '70%'
        }}
      >
        {chatMassages.length !== 0 && (
          <ListOfMassages
            chatMassages={chatMassages}
            jwt={jwt}
            hasMore={hasMore}
            openedChatId={openedChatId}
          />
        )}
      </Col>
      {lastActivity && (
        <span style={{ fontSize: 12 }}>
          Последняя активность: {lastActivity}
        </span>
      )}
      <Row>
        <Col>
          <SandMassageForm />
        </Col>
        <Col>Или выберите из готовых: </Col>
        <select>
          <option> 123 </option>
        </select>
      </Row>
    </>
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dialog);
