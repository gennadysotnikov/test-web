import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getSearchResult } from '../actions/chat/chat';

const mapStateToProps = state => ({
  jwt: state.reducerAuth.jwt
});
const mapDispatchToProps = dispatch => ({
  onSearch: (searchStr, jwt) => {
    //тут можно вставить lodash
    dispatch(getSearchResult(searchStr, jwt));
  }
});

function SearchInput({ onSearch, jwt }) {
  function onChange(event) {
    const searchStr = event.target.value;
    //debounce - задержка
    _.debounce(() => {
      onSearch(searchStr, jwt);
    }, 300)(_);
  }
  return <input id="SearchInput" type="text" onChange={onChange} />;
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchInput);
