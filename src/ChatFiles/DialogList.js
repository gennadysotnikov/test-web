import React from 'react';
import { getChatQuery, showMassages } from '../actions/chat/chat';
import { store } from '../store/store';
import InfiniteScroll from 'react-infinite-scroll-component';
import LiWithDispatch from '../components/LiWithDispatch';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  chats: state.reducerChat.chats,
  hasMore: state.reducerChat.hasMore,
  jwt: state.reducerAuth.jwt,
  showChatStatus: state.reducerChat.showChatStatus
});

const mapDispatchToProps = dispatch => ({
  onGetChat: (jwt, lastIndex, status, chats) => {
    dispatch(getChatQuery(jwt, lastIndex, status, chats));
  }
});

const mapDispatchToPropsForLi = dispatch => ({
  onClick: chatId => {
    dispatch(showMassages([], chatId));
  }
});
function DialogList({ chats, jwt, hasMore, onGetChat, showChatStatus }) {
  const idSource = 'chatId';
  const actions = mapDispatchToPropsForLi(store.dispatch);
  const loadChats = () => {
    onGetChat(jwt, chats[chats.length - 1].chatId, showChatStatus, chats);
  };
  alert('DialogList Рендер');
  return (
    <ul id="list_of_dialogs" style={{ height: '100%', overflow: 'auto' }}>
      <InfiniteScroll
        dataLength={chats.length}
        hasMore={hasMore}
        next={loadChats} //другая функция
        scrollableTarget="list_of_dialogs"
        upScroll={false}
      >
        {chats.map((item, index) => {
          return (
            item.status === item.status && (
              <LiWithDispatch
                client={item.userName}
                topic={item.name}
                key={idSource !== undefined ? item[idSource] : index}
                id={idSource !== undefined ? item[idSource] : index}
                content={item['name']}
                actions={actions}
                actionName={'onClick'}
                jwt={jwt}
                status={item.status}
              />
            )
          );
        })}
      </InfiniteScroll>
    </ul>
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DialogList);
