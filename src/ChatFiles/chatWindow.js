import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  massageToggle,
  searchToggle,
  toggleCreateChat
} from '../actions/chat/chat';
import '../сss_modules/auth.module.scss';
import CreateChatForm from './forms/createChatForm';
import DialogList from './DialogList';
import SearchResult from '../components/searchResult';
import Dialog from './dialog';

const mapStateToProps = state => ({
  chats: state.reducerChat.chats,
  jwt: state.reducerAuth.jwt,
  VisibleChatWindow: state.reducerChat.VisibleChatWindow,
  VisibleCreateChat: state.reducerChat.VisibleCreateChat,
  isSearch: state.reducerChat.isSearch,
  searchResult: state.reducerChat.searchResult,
  notFoundMassage: state.reducerChat.notFoundMassage,
  showChat: state.reducerChat.showChat,
  showChatStatus: state.reducerChat.showChatStatus,
  hasMore: state.reducerChat.hasMore
});
const mapDispatchToProps = dispatch => ({
  onCreateChatCall: () => {
    dispatch(toggleCreateChat(false, true));
  },
  onStopSearch: () => {
    dispatch(searchToggle(false));
  },
  onNotFound: () => {
    dispatch(massageToggle(true));
  },
  onFound: () => {
    dispatch(massageToggle(false));
  }
});
function ChatWindow({
  VisibleChatWindow,
  VisibleCreateChat,
  isSearch,
  searchResult,
  onStopSearch,
  onNotFound,
  onFound,
  notFoundMassage,
  showChat,
  showChatStatus
}) {
  useEffect(() => {
    if (document.getElementById('SearchInput') !== null)
      if (document.getElementById('SearchInput').value === '') onStopSearch();
      else {
        if (
          searchResult.nameMatches.length === 0 &&
          searchResult.massageMatches.length === 0
        )
          onNotFound();
        else onFound();
      }
  }, [
    VisibleCreateChat,
    VisibleChatWindow,
    isSearch,
    searchResult,
    showChatStatus
  ]);
  const chatWindow = (
    <div
      style={{
        height: '100%',
        width: '100%'
      }}
    >
      {VisibleChatWindow && !isSearch && showChatStatus !== 'none' && (
        <DialogList />
      )}
      {VisibleCreateChat && <CreateChatForm />}
      {isSearch && !notFoundMassage && (
        <SearchResult
          nameMatches={searchResult.nameMatches}
          massageMatches={searchResult.massageMatches}
          userNameMatches={searchResult.userNameMatches}
        />
      )}
      {showChat && <Dialog />}
      {notFoundMassage && isSearch && 'Ничего не найдено'}
    </div>
  );
  return chatWindow;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatWindow);
