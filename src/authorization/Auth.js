import AuthForm from './forms/authForm';
import React from 'react';
import { action } from '../actions/auth';
import ResultOfAuth from '../containers/ResultOfAuth';
import { auth } from '../сss_modules/auth.module.scss';
import GoogleRegOrAuth from '../components/GoogleRegOrAuth';
import VKRegOrAuth from '../components/VKRegOrAuth';

function Auth() {
  return (
    <div className={auth}>
      <AuthForm onSubmit={action.auth()} />
      <ResultOfAuth /> {/*Вероятно, компонент не нужен*/}
      <GoogleRegOrAuth />
      <VKRegOrAuth />
    </div>
  );
}
export default Auth;
