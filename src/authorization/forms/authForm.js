import React from 'react';
import { reduxForm, Field } from 'redux-form';
import renderField from '../../components/renderField';
import { submit } from '../../actions/auth';
import { Link } from 'react-router-dom';

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Обязательно';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email';
  }
  if (!values.password) {
    errors.password = 'Обязательно';
  }
  return errors;
};
function AuthForm({ handleSubmit, error }) {
  return (
    <form onSubmit={handleSubmit(submit)} method="POST">
      <label>Email</label>
      <Field name="email" component={renderField} type="text" />
      <label>Пароль</label>
      <Field name="password" component={renderField} type="password" />
      {error && <strong>{error}</strong>}
      <button type="submit" style={{ float: 'left' }}>
        Войти
      </button>
      <Link to={'/registration'}>
        <button>Регистрация</button>
      </Link>
    </form>
  );
}

export default reduxForm({ form: 'my_form', validate })(AuthForm);
