import React from 'react';
import { reduxForm, Field, Form, SubmissionError } from 'redux-form';
import renderField from '../../components/renderField';
import { auth } from '../../сss_modules/auth.module.scss';
import '../../сss_modules/auth.module.scss';
import { baseUrlUser } from '../../queryConfig';
import { queryWithTextOutput } from '../../jwtAuthFunctions';
import action from '../../actions/auth';
import { Link } from 'react-router-dom';

export const registration = values => {
  let url = baseUrlUser + '/registration';
  const req = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
      email: values.email,
      username: values.userName,
      password: values.password
    }
  };
  alert(JSON.stringify(req));
  return queryWithTextOutput(url, req).then(res => {
    switch (res) {
      case 'Not found':
        /* Неизвесная ошибка*/ break;
      case 'Internal server error':
        /* Ошибка на сервере. Возможно ведутся работы. */ break;
      case 'Fail: email is used':
        throw new SubmissionError({
          email: 'Пользователь с такой почтой уже зарегистрирован'
        });
      case 'Fail: username is used':
        throw new SubmissionError({
          userName: 'Пользователь с такой именем уже зарегистрирован'
        });
      default:
        action.success(res);
    }
  });
};

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Обязательно';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email';
  }
  if (!values.userName) {
    errors.userName = 'Обязательно';
  }
  if (!values.password) {
    errors.password = 'Обязательно';
  }
  if (!values.passwordSubmission) {
    errors.passwordSubmission = 'Обязательно';
  } else if (values.passwordSubmission !== values.password) {
    errors.passwordSubmission = 'Пароли не совпадают';
  }

  return errors;
};
function RegistrForm({ error, handleSubmit }) {
  return (
    <Form onSubmit={handleSubmit(registration)} className={auth}>
      <h2> Регистрация </h2>
      <label>Email</label>
      <Field name="email" key="email" component={renderField} type="text" />
      <label>Имя пользователя</label>
      <Field name="userName" component={renderField} type="text" />
      <label>Пароль</label>
      <Field name="password" component={renderField} type="password" />
      <label style={{ fontSize: '12pt' }}>Подтверждение пароля</label>
      <Field
        name="passwordSubmission"
        component={renderField}
        type="password"
      />
      {error && <strong>{error}</strong>}
      <button
        type="submit"
        style={{
          width: '100%',
          marginLeft: '0%',
          marginTop: '10px',
          fontSize: '13pt'
        }}
      >
        Зарегистрироваться
      </button>
      <Link to="/">
        <button style={{ float: 'left', marginRight: '30%' }}>Войти</button>
      </Link>
      <h6> Забыли пароль? </h6>
    </Form>
  );
}

export default reduxForm({
  form: 'registrForm',
  validate
})(RegistrForm);
