import React from 'react';
import { Field, Form, reduxForm } from 'redux-form';
import renderField from '../../components/renderField';
import { auth } from '../../сss_modules/auth.module.scss';
import '../../сss_modules/auth.module.scss';
import { Link } from 'react-router-dom';

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = 'Обязательно';
  }
  if (!values.passwordSubmission) {
    errors.passwordSubmission = 'Обязательно';
  } else if (values.passwordSubmission !== values.password) {
    errors.passwordSubmission = 'Пароли не совпадают';
  }

  return errors;
};

const PasswordResetForm = () => (
  <Form
    onSubmit={
      {
        /*функция, которая отправит данные для обновления пароля*/
      }
    }
    className={auth}
  >
    <h2> Обновить пароль </h2>
    <label>Пароль</label>
    <Field name="password" component={renderField} type="password" />
    <label style={{ fontSize: '12pt' }}>Подтверждение пароля</label>
    <Field name="passwordSubmission" component={renderField} type="password" />
    <button
      type="submit"
      style={{
        width: '100%',
        marginLeft: '0%',
        marginTop: '10px',
        fontSize: '13pt'
      }}
    >
      Отправить ссылку для восстановления
    </button>
    <Link to="/">
      <button style={{ float: 'left', marginRight: '30%' }}>Войти</button>
    </Link>
    <Link to={'/registration'}>
      <button style={{ marginTop: '3%', float: 'left', marginRight: '3.1%' }}>
        Регистрация
      </button>
    </Link>
  </Form>
);

export default reduxForm({
  form: 'passwordResetForm',
  validate
})(PasswordResetForm);
