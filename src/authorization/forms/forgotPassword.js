import { auth } from '../../сss_modules/auth.module.scss';
import { Field, Form, reduxForm } from 'redux-form';
import renderField from '../../components/renderField';
import { Link } from 'react-router-dom';
import React from 'react';

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Обязательно';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email';
  }
  return errors;
};

const ForgotPasswordForm = () => (
  <Form
    onSubmit={
      {
        /*js функция*/
      }
    }
    className={auth}
  >
    <h2> Регистрация </h2>
    <label>Email</label>
    <Field name="email" key="email" component={renderField} type="text" />
    <button
      type="submit"
      style={{
        width: '100%',
        marginLeft: '0%',
        marginTop: '10px',
        fontSize: '13pt'
      }}
    >
      Отправить ссылку для восстановления
    </button>
    <Link to="/">
      <button style={{ float: 'left', marginRight: '30%' }}>Войти</button>
    </Link>
    <Link to={'/registration'}>
      <button style={{ marginTop: '3%', float: 'left', marginRight: '3.1%' }}>
        Регистрация
      </button>
    </Link>
  </Form>
);
export default reduxForm({
  form: 'forgotPasswordForm',
  validate
})(ForgotPasswordForm);
