import React from 'react';
import { connect } from 'react-redux';
let massage;
const mapStateToProps = state => {
  //state.reducerAuth.massage возвращает '345' - initial state
  massage = state.reducerAuth.massage;
  return { massage: massage };
};
//Компонент не обновляется с изменением massage
const ResultOfAuth = () => {
  //точно известно, что massage выводится
  return <p>{massage}</p>;
};
export default connect(
  mapStateToProps,
  null
)(ResultOfAuth);
