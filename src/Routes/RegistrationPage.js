import React from 'react';
import { Redirect, Route } from 'react-router';
import RegistrForm from '../authorization/forms/registerForm';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  auth: state.reducerAuth.auth
});
const RegistrationPage = ({ auth }) => {
  return (
    <Route
      exact
      path="/registration"
      render={() => (auth ? <Redirect to={'/'} /> : <RegistrForm />)}
    />
  );
};

export default connect(
  mapStateToProps,
  null
)(RegistrationPage);
