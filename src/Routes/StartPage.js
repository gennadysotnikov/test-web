import React, { useEffect } from 'react';
import { firstCheckQuery } from '../actions/auth';
import Auth from '../authorization/Auth';
import { connect } from 'react-redux';
import ExitButton from '../components/buttons/ExitButton';
import ChatWindow from '../ChatFiles/chatWindow';
import { Container, Row, Col } from 'reactstrap';
import SearchInput from '../ChatFiles/searchInput';
import { sp } from '../сss_modules/auth.module.scss';
import { exitChat, toggleShowChatStatus } from '../actions/chat/chat';
import { store } from '../store/store';

const mapStateToProps = state => ({
  auth: state.reducerAuth.auth,
  jwt: state.reducerAuth.jwt,
  VisibleChatWindow: state.reducerChat.VisibleChatWindow,
  VisibleCreateChat:
    state.reducerChat.VisibleCreateChat /*возможно пригодится позже*/,
  isSearch: state.reducerChat.isSearch,
  showChat: state.reducerChat.showChat
});
const mapDispatchToProps = dispatch => ({
  onExit: () => {
    dispatch(exitChat());
  },
  onFirstAuth: () => {
    dispatch(firstCheckQuery);
  }
});

function StartPage({
  auth,
  jwt,
  onFirstAuth,
  showChat,
  /*isSearch,*/
  VisibleChatWindow,
  onCreateChatCall,
  onExit
}) {
  useEffect(() => {
    onFirstAuth();
  });
  if (auth) {
    return (
      <Container
        className={sp}
        style={{
          display: 'flex',
          height: '500px',
          flexDirection: 'inherit'
        }}
      >
        <Row>
          <Col style={{ height: '100%' }} xs="4">
            <Col
              onClick={() =>
                store.dispatch(toggleShowChatStatus('active', jwt, []))
              }
            >
              Активные
            </Col>
            <Col
              onClick={() =>
                store.dispatch(toggleShowChatStatus('completed', jwt, []))
              }
            >
              Завершенные
            </Col>
            <Col
              onClick={() =>
                store.dispatch(toggleShowChatStatus('saved', jwt, []))
              }
            >
              Сохраненные
            </Col>
            <Row heigth="100%">
              <Col style={{ marginTop: '90%' }}>
                {!showChat && VisibleChatWindow && (
                  <button value="Создать чат" onClick={onCreateChatCall}>
                    Создать чат
                  </button>
                )}
              </Col>
            </Row>
            {/*Колонка с кнопками "активные"*, "завершенные", "Сохраненные" */}
          </Col>
          <Col xs="8" style={{ height: '100%' }}>
            <Row>
              <Col xs="8" style={{ height: '100%', textAlign: 'right' }}>
                operator@mail.ru
              </Col>
              <Col>
                <ExitButton />
              </Col>
            </Row>
            <Row>
              {showChat && (
                <Col sm="2">
                  <button onClick={onExit}> Назад </button>
                </Col>
              ) /*Должно быть имя чата, или клиента*/}
              {showChat && (
                <Col> Клиент 1 </Col>
              ) /*Должно быть имя чата, или клиента*/}
              <Col align="right">
                Поиск
                <SearchInput jwt={jwt} />
              </Col>
            </Row>
            <Row style={{ height: '70%' }}>
              <Col style={{ height: '100%' }}>
                <ChatWindow />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  } else {
    return <Auth />;
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartPage);
